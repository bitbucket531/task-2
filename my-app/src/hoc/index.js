import RectangleResult from "../components/RectangleResult";
import withCustomRect from "./withCustomRect";
import components from "../components";

components.RectangleResult = withCustomRect((props) => (
  <RectangleResult {...props} />
));

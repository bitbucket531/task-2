import React, { useState, useEffect } from "react";
import styled from "styled-components";
import "./styles.css";

const MyBall = styled.div`
  width: ${20 + "px"};
  height: ${20 + "px"};
  position: absolute;
  left: ${(props) => props.x + "px"};
  top: ${(props) => props.y + "px"};
  background-color: black;
  border-radius: 50%;
  transition: all 0.5s;
`;

const Ball = () => {
  const [pos, changePos] = useState(0);
  const [dir, changeDir] = useState(true);
  const [running, startRun] = useState(true);

  const positions = [
    { x: 0, y: 0 },
    { x: 200, y: 200 },
    { x: 400, y: 0 },
    { x: 600, y: 200 },
    { x: 800, y: 0 },
  ];

  useEffect(() => {
    const interval = setInterval(() => {
     handlePosition();
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  const handlePosition = () => {
    if (dir) {
      if (pos + 1 == positions.length) {
        console.log("going backward");
        changeDir(false);
        changePos(pos - 1);
      } else {
        console.log("going forward");
        changePos(pos + 1);
      }
    } else {
      if (pos - 1 < 0) {
        console.log("going forward");
        changeDir(true);
        changePos(pos + 1);
      } else {
        console.log("going backward");
        changePos(pos - 1);
      }
    }
  };

  const startRunning = () => {
    startRun(true);
  };

  return (
    <div className="ball__wrapper">
      <MyBall className="ball" x={positions[pos].x} y={positions[pos].y} />
      <button onClick={() => handlePosition()}>Pop!</button>
    </div>
  );
};

export default Ball;

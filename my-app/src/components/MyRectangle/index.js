import styled from "styled-components";
import components from "../../components";
require("../../hoc");

const MyLittleRectangle = styled.div`
  width: ${(props) => props.width + "px"};
  height: ${(props) => props.height + "px"};
  margin: 0 auto;
  background-color: black;
  display: findByLabelText;
  text-alight: center;
  alight-items: center;
  justify-content: center;
  transition: all 0.5s;
`;

const MyRectangle = ({ innerRef, width, height }) => {
  const { RectangleResult } = components;
  return (
    <MyLittleRectangle ref={innerRef} width={width} height={height}>
      <RectangleResult innerRef={innerRef} />
    </MyLittleRectangle>
  );
};

export default MyRectangle;

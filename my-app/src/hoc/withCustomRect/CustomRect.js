const CustomRect = ({ innerRef }) => {
  var myWidth = 100;
  var myHeight = 100;
  var myPadding =new String(myHeight+"px");
  if (innerRef.current !== undefined) {
      myWidth = innerRef.current.offsetWidth;
      myHeight = innerRef.current.offsetHeight;
      const className = innerRef.current.className;
      const element = document.getElementsByClassName(className)[0];
      element.style.borderRadius ="100%";
      myPadding = (myHeight)+"px";
    }
    console.log(myPadding)
  return (
    <p style={{ color: "white",paddingTop:{myPadding}}}>
      Pole:{myWidth}*{myHeight}={myWidth * myHeight}
    </p>
  );
};

export default CustomRect;

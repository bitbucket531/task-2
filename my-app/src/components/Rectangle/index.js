import React, { Component, useEffect, useRef, useState } from "react";
import MyRectangle from "../MyRectangle";

const Rectangle = () => {
  const HandleResize = (max, min) => {
    setWidth(Math.floor(Math.random() * (max - min) + min));
    setHeight(Math.floor(Math.random() * (max - min) + min));
  };

  const myRef = useRef();
  const [width, setWidth] = useState(100);
  const [height, setHeight] = useState(100);
  const max = 400;
  const min = 100;
  return (
    <div>
      <p>Width: {width}px</p>
      <p>Height: {height}px</p>
      <button
        onClick={() => HandleResize(max, min)}
        style={{ marginBottom: "10px" }}
      >
        Generate new element
      </button>
      <MyRectangle innerRef={myRef} width={width} height={height} />
    </div>
  );
};

export default Rectangle;

const RectangleResult = ({ innerRef }) => {
  var myWidth = 100;
  var myHeight = 100;

  if (innerRef.current !== undefined) {
    myWidth = innerRef.current.offsetWidth;
    myHeight = innerRef.current.offsetHeight;
    const className = innerRef.current.className;
    const element = document.getElementsByClassName(className)[0];
    element.style.borderRadius ="0";
  }
  return (
    <p style={{ color: "white" }}>
      Pole:{myWidth}*{myHeight}={myWidth * myHeight}
    </p>
  );
};

export default RectangleResult;

import React, { useEffect, useState } from "react";
import CustomRect from "./CustomRect";

const withCustomRect = (Component) => (props) => {
  const [count, setCount] = useState(0);
  useEffect(() => {
    setCount(count + 1);
    console.log(count);
  },[props]);

  return (
    <div>{
        (count%2==0) ? (
      <CustomRect innerRef={props.innerRef} />) : (
      <Component innerRef={props.innerRef} />)
    }
    </div>
  );
};

export default withCustomRect;
